import socket

server_address = (('localhost', 8080))

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(server_address)
server.settimeout(1000)
server.listen()

message = 'I am SERVER\n'
recibidos = 0

while True:
	print('Server disponible!')
	connection, client_address = server.accept()
	from_client = ''
	while True:
		data = connection.recv(4096)
		if not data: break
		from_client += data.decode()
		recibidos += 1
		send = 'mensajes recibidos: ' + str(recibidos)
		connection.send(send.encode('utf-8'))
		print(from_client)

	connection.close()
	print('cliente desconectado \n')
	recibidos = 0
