import socket

class Client:
  def __init__(self, adapter=None):
    self.adapter = adapter 
  
  def connect(self):
    self.adapter.connect()
  
  def disconnect(self):
    self.adapter.disconnect()

  def send(self, message):
    self.adapter.send(message)

  def recieve(self):
    return self.adapter.recieve()