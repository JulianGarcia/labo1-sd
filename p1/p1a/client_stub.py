import socket

class Stub:
  def __init__(self, host='0.0.0.0', port=8080):
    self.host = host
    self.port = port
  
  def connect(self):
    self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.client.connect((self.host, self.port))
  
  def disconnect(self):
    self.client.close()

  def send(self, message):
    self.client.send(message.encode('utf-8'))

  def recieve(self):    
    _msg = self.client.recv(4096)
    msg = _msg.decode()
    return msg