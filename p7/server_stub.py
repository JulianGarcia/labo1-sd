import socket
import pickle
import file_system_structures
from concurrent import futures
import time
import threading
import collections

# original file_system.py
from file_system import FS

POOL_SIZE = 10
POOL = []

class Thread(threading.Thread):

  def __init__(self, threadID):
    self.threadID = threadID
    threading.Thread.__init__(self)
  
  def run(self):
    print("soy un hilo: ", self.threadID)
    while True:
      if POOL[self.threadID][0]._process_request():
        break
      
class ServerFSStub():

  def __init__(self, channel, file_system_adapter):
    self._channel = channel
    self._adapter = file_system_adapter

  def _process_request(self):
    print('process request')
    pickle_data = self._channel.recv(4096)
    if pickle_data:
      path = pickle.loads(pickle_data)
      if path is not None:
        if path.operacion == 1:
          _path = path.value
          path_files = FS.list_files(_path)
          for _path in path_files:
            self._channel.sendall(_path)
        elif path.operacion == 2:
          path_read_value = FS.read_file(path)
          pickle_read_value = pickle.dumps(path_read_value)
          self._channel.sendall(pickle_read_value)
      return 0
    else:
      return 1

  
class ServerStub:
  def __init__(self, adapter, port=8090):
    self._port = port
    self._adapter = adapter
    self._stub = None
    self.server = None

  def _setup(self):
    self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(self._port)
    self.server.bind(("0.0.0.0", self._port)) 

  def run(self):
    self._setup()
    self.setup_pool(POOL_SIZE)
    self.server.listen()
    number_of_thread = 0
    t = 0
    try:
      while True:
        connection, client_address = self.server.accept()
        from_client = ''
        self._stub = ServerFSStub(connection, self._adapter)
        import pdb; pdb.set_trace()
        for t in POOL:
          if not t[2]:
            #con [0] se hace referencia al hilo, con [1] se hace referencia al estado
            t[0] = self._stub
            t[2] = True
            _t = t[1].threadID
            t[1].start()
            t[2] = False
            t[1] = Thread(_t)
    except KeyboardInterrupt:
      connection.close()
      self.server.stop(0)
      self.pool = []
    finally:
      connection.close()

  def setup_pool(self, number_threads):
    t = 0
    try:
      while t <= number_threads:
        p = list((None, Thread(t), False))
        POOL.append(p)
        t += 1

    except Exception as e:
      print('Pool initialize error')
