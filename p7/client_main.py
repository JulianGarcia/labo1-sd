from client import Client
from client_stub import ClientStub

HOST = 'localhost'
PORT = 8090
#FILE = '/home/julian/sistemas-distribuidos/Práctica/practica1/file.txt'
#FILE = '/home/julian/sistemas-distribuidos/tamaños.txt'
FILE = '/home/julian/sistemas-distribuidos/asd.pdf'

def main():
    stub = ClientStub(HOST, PORT)
    client = Client(HOST, PORT, stub)
    client.connect()
    #response = client.list_files('/home/julian')
    response = client.read_file(FILE)
    

if __name__ == "__main__":
    main()
    

